from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app) # open interpreter: from main import db; db.create_all(); exit();
ma = Marshmallow(app)
api = Api(app)

class IOC(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    git = db.Column(db.String(255))
    version = db.Column(db.String(255))

    def __repr__(self):
        return f'<IOC {self.id}, {self.name}'

class IOCSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "git", "version")
        model = IOC
    
ioc_schema = IOCSchema()
iocs_schema = IOCSchema(many=True)

class IOCListResource(Resource):
    def get(self):
        iocs = IOC.query.all()
        return iocs_schema.dump(iocs)
    
    def post(self):
        new_ioc = IOC(
            name=request.json['name'],
            git=request.json['git'],
            version = request.json['version']
        )
        db.session.add(new_ioc)
        db.session.commit()
        return ioc_schema.dump(new_ioc)

api.add_resource(IOCListResource, '/iocs')

class IOCResource(Resource):
    def get(self, ioc_id):
        ioc = IOC.query.get_or_404(ioc_id)
        return ioc_schema.dump(ioc)

    def patch(self, ioc_id):
        ioc = IOC.query.get_or_404(ioc_id)
        if 'name' in request.json:
            ioc.name = request.json['name']
        if 'version' in request.json:
            ioc.version = request.json['version']
        if 'git' in request.json:
            ioc.git = request.json['git']
        db.session.commit()
        return ioc_schema.dump(ioc)
    
    def delete(self, ioc_id):
        ioc = IOC.query.get_or_404(ioc_id)
        db.session.delete(ioc)
        db.session.commit()
        return '', 204

api.add_resource(IOCResource, '/iocs/<int:ioc_id>')

if __name__ == '__main__':
    app.run(debug=True)