# ccce-demo

demo of ccce functionality

To run server:
```
cd server
python main.py
```
To use server rest API:
```
# Create an IOC
curl http://localhost:5000/iocs -X POST -H "Content-Type: application/json" -d '{"name": "my-ioc", "git": "http://git.com/my-ioc", "version": "master"}'
{
    "id": 1,
    "name": "my-ioc",
    "git": "http://git.com/my-ioc",
    "version": "master"
}

# Update an IOC
curl http://localhost:5000/iocs/1 -X PATCH -H "Content-Type: application/json" -d '{"git": "http://git.com/my-ioc.git"}'
{
    "id": 1,
    "name": "my-ioc",
    "git": "http://git.com/my-ioc.git",
    "version": "master"
}

# Get a list of all IOCS
curl http://localhost:5000/iocs
[
    {
        "id": 1,
        "name": "my-ioc",
        "git": "http://git.com/my-ioc.git",
        "version": "master"
    }
]

# Get a single IOC by id
curl http://localhost:5000/iocs/1
{
    "id": 1,
    "name": "my-ioc",
    "git": "http://git.com/my-ioc.git",
    "version": "master"
}


# Delete an IOC
curl http://localhost:5000/iocs/1 -X DELETE -I
HTTP/1.0 204 NO CONTENT
Content-Type: application/json
Server: Werkzeug/1.0.1 Python/3.7.6
Date: Tue, 05 Jan 2021 20:30:56 GMT
```